import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            apiResponse: '',
            displayFilterByList: false,
            selectedFilter: 'Click Me',
            filteredList: []
        };

        this.showFilterDropdownList = this.showFilterDropdownList.bind(this);
        this.hideFilterDropdownList = this.hideFilterDropdownList.bind(this);
    }

    showFilterDropdownList(event) {
        event.preventDefault();
        this.setState({displayFilterByList: true}, () => {
           document.addEventListener('click', this.hideFilterDropdownList);
        });
    }

    hideFilterDropdownList(event) {
        this.setState({displayFilterByList: false}, () => {
           document.removeEventListener('click', this.hideFilterDropdownList);
        });
    }

    callApi() {
        fetch("http://localhost:9000/productsApi/products")
            .then(res => res.json())
            .then(res => this.setState({ apiResponse: res }))
            .catch(err => err);
    }

    renderTableData() {
        const filteredList = this.state.filteredList.length === 0 ? this.state.apiResponse : this.state.filteredList;
        return filteredList.map((product, index) => {
            const {
                id,
                description,
                lastSold,
                shelfLife,
                department,
                price,
                unit,
                xFor,
                cost
            } = product;

            return (
                <tr key={id} className="table-row">
                    <td>{id}</td>
                    <td>{description}</td>
                    <td>{lastSold}</td>
                    <td>{shelfLife}</td>
                    <td>{department}</td>
                    <td>{price}</td>
                    <td>{unit}</td>
                    <td>{xFor}</td>
                    <td>{cost}</td>
                </tr>
            )
        })
    }

    renderTableHeader() {
        let header = Object.keys(this.state.apiResponse[0]);
        return header.map((key, index) => {
            if(key !== '_id')
                return <th key={index}>{key}</th>
        });
    }

    renderSearchComponent() {
        return (
            <div>
                <input
                    className="searchBox"
                    type="text"
                    placeholder="Search"
                    onKeyPress={this.filterList.bind(this)}
                />
                <div className="dropdown">
                    <div className="button" onClick={this.showFilterDropdownList}>{this.state.selectedFilter}</div>
                    { this.state.displayFilterByList ? (
                    <ul>
                        {this.getProductFilterByList()}
                    </ul>
                        ) : (null)
                    }
                </div>
            </div>
        )
    }

    getProductFilterByList() {
        let productKeys = Object.keys(this.state.apiResponse[0]);
        return productKeys.map((key, index) => {
            if(key !== '_id')
                return <li key={index} onClick={this.handleFilterSelected.bind(this)}>{key}</li>
        });
    }

    handleFilterSelected(event) {
        this.setState({selectedFilter: event.target.textContent}, () => {
            document.removeEventListener('click', this.handleFilterSelected);
        });
    }

    componentDidMount() {
        this.callApi();
    }

    filterList = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            const {
                apiResponse,
                selectedFilter,
                filteredList
            } = this.state;

            const searchInput = event.target.value;

            if(selectedFilter !== 'Click Me') {
                let items = apiResponse.filter((product, index) => {
                    if(product[selectedFilter].toLowerCase().includes(searchInput.toLowerCase())) {
                        return product[selectedFilter];
                    }
                });

                this.setState({filteredList: items});
            }
        }
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to Market Products</h1>
                </header>
                <form>
                    {this.state.apiResponse && this.renderSearchComponent()}
                </form>
                <table className="App-table">
                    <tbody>
                        <tr className="table-row">{this.state.apiResponse && this.renderTableHeader()}</tr>
                        {this.state.apiResponse && this.renderTableData()}
                    </tbody>
                </table>
                <footer className="App-footer">

                </footer>
            </div>
        );
    }
}

export default App;
