let mongoose = require('mongoose');
const Decimal = require('mongoose');
const mongodb = require("mongodb");
const csvtojson = require('csvtojson');

const url = "mongodb://mongodb:27017/test";

function getProductSchema() {
    return mongoose.Schema({
        id: Number,
        description: String,
        lastSold: Date,
        shelfLife: String,
        department: String,
        price: String,
        unit: String,
        xFor: String,
        cost: String
    });
}

let Product = mongoose.model('productsModel', getProductSchema());

csvtojson()
    .fromFile(__dirname + '/../data/testdata.csv')
    .then(csvData => {
        // Connecting to MongoDB
        mongodb.connect(
            url,
            {useNewUrlParser: true, useUnifiedTopology: true},
            (err, client) => {
                if (err) throw err;

                client
                    .db('products-db')
                    .collection('products')
                    .insertMany(csvData, (err, res) => {
                        if(err) throw err;

                        console.log(`Inserted: ${res.insertedCount} rows`);
                        client.close();
                    });
            }
        );
    });

const getProducts = () => {
    mongodb.connect(url, (err, client) => {
        let dbPromise = () => {
            return new Promise((resolve, reject) => {
                client
                    .db('products-db')
                    .collection('products')
                    .find({}).toArray((err, res) => {
                    if (err) reject(err);
                    resolve(res);
                });
            });
        };

        let asyncDbGet = async () => {
            return await (dbPromise());
        };

        asyncDbGet().then(function (result) {
            client.close();
            return result;
        })
    });
};

exports.getProducts = getProducts;