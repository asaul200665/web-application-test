const {getProducts} = require("../public/javascripts/products-dao");
const express = require('express');
const router = express.Router();
const mongodb = require("mongodb");

const url = "mongodb://mongodb:27017/test";

router.get('/products', function(req, res, next) {
    // I have this code in the dao but it's not returning what I would expect.
    // something something something, just trying to get it to work at this point.
    mongodb.connect(url, (err, client) => {
        let dbPromise = () => {
            return new Promise((resolve, reject) => {
                client
                    .db('products-db')
                    .collection('products')
                    .find({}).toArray((err, res) => {
                    if (err) reject(err);
                    resolve(res);
                });
            });
        };

        let asyncDbGet = async () => {
            return await (dbPromise());
        };

        asyncDbGet().then(function (result) {
            client.close();
            res.json(result);
        })
    });
});

module.exports = router;