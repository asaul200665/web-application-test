const express = require("express");
const router = express.Router();

// Variable to be sent to Frontend with Database status
let databaseConnection = "Waiting for Database response...";

router.get('/', function(req, res, next) {
    res.send(databaseConnection);
});

module.exports = router;