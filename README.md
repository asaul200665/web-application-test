# web-application-test

The following is a solution to a simple CSV db application.  
My intent was to provide a deliverable that can be started up with a few simple commands.

## Dependencies
### Docker
 Docker is needed to locally start up the application.  The latest will do.  
 I personally developed on a windows machine, but the use of docker should enable cross compatibility

## How to execute

- After downloading the project navigate to the `web-application-test` folder
- Run `docker-compose build` this will build the local images 
- Run `docker-compose up` which starts up the 3 containers associated with the application
- open your favorite browser and navigate to `localhost:3000`
- If you have to restart the docker container make sure to execute `docker-compose down` so mongodb instance can be flushed